
>
Refreshing IP repositories
234*coregenZ19-234h px� 
�
 Loaded user IP repository '%s'.
1135*coregen2A
-/home/gderiu/Desktop/vivado-library-master/ip2default:defaultZ19-1700h px� 
~
"Loaded Vivado IP repository '%s'.
1332*coregen25
!/opt/Xilinx/Vivado/2019.1/data/ip2default:defaultZ19-2313h px� 
�
Command: %s
53*	vivadotcl2j
Vsynth_design -top design_1_fir_compiler_0_0 -part xc7z010clg400-1 -mode out_of_context2default:defaultZ4-113h px� 
:
Starting synth_design
149*	vivadotclZ4-321h px� 
�
@Attempting to get a license for feature '%s' and/or device '%s'
308*common2
	Synthesis2default:default2
xc7z0102default:defaultZ17-347h px� 
�
0Got license for feature '%s' and/or device '%s'
310*common2
	Synthesis2default:default2
xc7z0102default:defaultZ17-349h px� 
V
Loading part %s157*device2#
xc7z010clg400-12default:defaultZ21-403h px� 
�
%s*synth2�
�Starting RTL Elaboration : Time (s): cpu = 00:00:08 ; elapsed = 00:00:14 . Memory (MB): peak = 1805.445 ; gain = 153.684 ; free physical = 2391 ; free virtual = 8158
2default:defaulth px� 
�
synthesizing module '%s'638*oasys2-
design_1_fir_compiler_0_02default:default2�
�/home/gderiu/AES2021/audio_lab/audio_lab/audio_lab.srcs/sources_1/bd/design_1/ip/design_1_fir_compiler_0_0/synth/design_1_fir_compiler_0_0.vhd2default:default2
732default:default8@Z8-638h px� 
f
%s
*synth2N
:	Parameter C_XDEVICEFAMILY bound to: zynq - type: string 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter C_ELABORATION_DIR bound to: ./ - type: string 
2default:defaulth p
x
� 
|
%s
*synth2d
P	Parameter C_COMPONENT_NAME bound to: design_1_fir_compiler_0_0 - type: string 
2default:defaulth p
x
� 
{
%s
*synth2c
O	Parameter C_COEF_FILE bound to: design_1_fir_compiler_0_0.mif - type: string 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter C_COEF_FILE_LINES bound to: 32 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter C_FILTER_TYPE bound to: 0 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter C_INTERP_RATE bound to: 1 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter C_DECIM_RATE bound to: 1 - type: integer 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter C_ZERO_PACKING_FACTOR bound to: 1 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_SYMMETRY bound to: 1 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter C_NUM_FILTS bound to: 2 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter C_NUM_TAPS bound to: 21 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter C_NUM_CHANNELS bound to: 2 - type: integer 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter C_CHANNEL_PATTERN bound to: fixed - type: string 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter C_ROUND_MODE bound to: 0 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter C_COEF_RELOAD bound to: 0 - type: integer 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter C_NUM_RELOAD_SLOTS bound to: 1 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_COL_MODE bound to: 1 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter C_COL_PIPE_LEN bound to: 4 - type: integer 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter C_COL_CONFIG bound to: 1 - type: string 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter C_OPTIMIZATION bound to: 0 - type: integer 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter C_DATA_PATH_WIDTHS bound to: 16 - type: string 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter C_DATA_IP_PATH_WIDTHS bound to: 16 - type: string 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter C_DATA_PX_PATH_WIDTHS bound to: 16 - type: string 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter C_DATA_WIDTH bound to: 16 - type: integer 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter C_COEF_PATH_WIDTHS bound to: 16 - type: string 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter C_COEF_WIDTH bound to: 16 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter C_DATA_PATH_SRC bound to: 0 - type: string 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter C_COEF_PATH_SRC bound to: 0 - type: string 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter C_PX_PATH_SRC bound to: 0 - type: string 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter C_DATA_PATH_SIGN bound to: 0 - type: string 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter C_COEF_PATH_SIGN bound to: 0 - type: string 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter C_ACCUM_PATH_WIDTHS bound to: 24 - type: string 
2default:defaulth p
x
� 
d
%s
*synth2L
8	Parameter C_OUTPUT_WIDTH bound to: 24 - type: integer 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter C_OUTPUT_PATH_WIDTHS bound to: 24 - type: string 
2default:defaulth p
x
� 
k
%s
*synth2S
?	Parameter C_ACCUM_OP_PATH_WIDTHS bound to: 24 - type: string 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter C_EXT_MULT_CNFG bound to: none - type: string 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter C_DATA_PATH_PSAMP_SRC bound to: 0 - type: string 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter C_OP_PATH_PSAMP_SRC bound to: 0 - type: string 
2default:defaulth p
x
� 
`
%s
*synth2H
4	Parameter C_NUM_MADDS bound to: 1 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter C_OPT_MADDS bound to: none - type: string 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter C_OVERSAMPLING_RATE bound to: 11 - type: integer 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter C_INPUT_RATE bound to: 150000 - type: integer 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter C_OUTPUT_RATE bound to: 150000 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter C_DATA_MEMTYPE bound to: 0 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter C_COEF_MEMTYPE bound to: 2 - type: integer 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter C_IPBUFF_MEMTYPE bound to: 0 - type: integer 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter C_OPBUFF_MEMTYPE bound to: 0 - type: integer 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter C_DATAPATH_MEMTYPE bound to: 0 - type: integer 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter C_MEM_ARRANGEMENT bound to: 1 - type: integer 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter C_DATA_MEM_PACKING bound to: 0 - type: integer 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter C_COEF_MEM_PACKING bound to: 0 - type: integer 
2default:defaulth p
x
� 
c
%s
*synth2K
7	Parameter C_FILTS_PACKED bound to: 0 - type: integer 
2default:defaulth p
x
� 
_
%s
*synth2G
3	Parameter C_LATENCY bound to: 21 - type: integer 
2default:defaulth p
x
� 
b
%s
*synth2J
6	Parameter C_HAS_ARESETn bound to: 0 - type: integer 
2default:defaulth p
x
� 
a
%s
*synth2I
5	Parameter C_HAS_ACLKEN bound to: 0 - type: integer 
2default:defaulth p
x
� 
e
%s
*synth2M
9	Parameter C_DATA_HAS_TLAST bound to: 0 - type: integer 
2default:defaulth p
x
� 
f
%s
*synth2N
:	Parameter C_S_DATA_HAS_FIFO bound to: 1 - type: integer 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter C_S_DATA_HAS_TUSER bound to: 0 - type: integer 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter C_S_DATA_TDATA_WIDTH bound to: 16 - type: integer 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter C_S_DATA_TUSER_WIDTH bound to: 1 - type: integer 
2default:defaulth p
x
� 
h
%s
*synth2P
<	Parameter C_M_DATA_HAS_TREADY bound to: 0 - type: integer 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter C_M_DATA_HAS_TUSER bound to: 0 - type: integer 
2default:defaulth p
x
� 
j
%s
*synth2R
>	Parameter C_M_DATA_TDATA_WIDTH bound to: 24 - type: integer 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter C_M_DATA_TUSER_WIDTH bound to: 1 - type: integer 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter C_HAS_CONFIG_CHANNEL bound to: 1 - type: integer 
2default:defaulth p
x
� 
g
%s
*synth2O
;	Parameter C_CONFIG_SYNC_MODE bound to: 0 - type: integer 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter C_CONFIG_PACKET_SIZE bound to: 0 - type: integer 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter C_CONFIG_TDATA_WIDTH bound to: 8 - type: integer 
2default:defaulth p
x
� 
i
%s
*synth2Q
=	Parameter C_RELOAD_TDATA_WIDTH bound to: 1 - type: integer 
2default:defaulth p
x
� 
�
Hmodule '%s' declared at '%s:%s' bound to instance '%s' of component '%s'3392*oasys2(
fir_compiler_v7_2_122default:default2�
|/home/gderiu/AES2021/audio_lab/audio_lab/audio_lab.srcs/sources_1/bd/design_1/ipshared/c2da/hdl/fir_compiler_v7_2_vh_rfs.vhd2default:default2
612642default:default2
U02default:default2(
fir_compiler_v7_2_122default:default2�
�/home/gderiu/AES2021/audio_lab/audio_lab/audio_lab.srcs/sources_1/bd/design_1/ip/design_1_fir_compiler_0_0/synth/design_1_fir_compiler_0_0.vhd2default:default2
2132default:default8@Z8-3491h px� 
�
�RAM %s from Abstract Data Type (record/struct) for this pattern/configuration is not supported. This will most likely be implemented in registers
4277*oasys2"
p_path_out_reg2default:defaultZ8-5858h px� 
�
%done synthesizing module '%s' (%s#%s)256*oasys2-
design_1_fir_compiler_0_02default:default2
142default:default2
12default:default2�
�/home/gderiu/AES2021/audio_lab/audio_lab/audio_lab.srcs/sources_1/bd/design_1/ip/design_1_fir_compiler_0_0/synth/design_1_fir_compiler_0_0.vhd2default:default2
732default:default8@Z8-256h px� 
�
!design %s has unconnected port %s3331*oasys2*
delay__parameterized132default:default2
SCLR2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2*
delay__parameterized132default:default2
SCLR_ALT2default:defaultZ8-3331h px� 
u
!design %s has unconnected port %s3331*oasys2
buff2default:default2
RE2default:defaultZ8-3331h px� 
w
!design %s has unconnected port %s3331*oasys2
buff2default:default2
SCLR2default:defaultZ8-3331h px� 
|
!design %s has unconnected port %s3331*oasys2
cntrl_delay2default:default2
CE2default:defaultZ8-3331h px� 
~
!design %s has unconnected port %s3331*oasys2
cntrl_delay2default:default2
SCLR2default:defaultZ8-3331h px� 
}
!design %s has unconnected port %s3331*oasys2
cntrl_delay2default:default2
CLK2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2*
delay__parameterized162default:default2
WE2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2*
delay__parameterized162default:default2
CE2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2*
delay__parameterized162default:default2
SCLR2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2*
delay__parameterized162default:default2
SCLR_ALT2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2*
delay__parameterized162default:default2
CLK2default:defaultZ8-3331h px� 
}
!design %s has unconnected port %s3331*oasys2
calc2default:default2

PRE_ADDSUB2default:defaultZ8-3331h px� 
v
!design %s has unconnected port %s3331*oasys2
calc2default:default2
CED2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2*
delay__parameterized152default:default2
WE2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2*
delay__parameterized152default:default2
CE2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2*
delay__parameterized152default:default2
SCLR2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2*
delay__parameterized152default:default2
SCLR_ALT2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2*
delay__parameterized152default:default2
CLK2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2*
delay__parameterized142default:default2
WE2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2*
delay__parameterized142default:default2
CE2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2*
delay__parameterized142default:default2
SCLR2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2*
delay__parameterized142default:default2
SCLR_ALT2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2*
delay__parameterized142default:default2
CLK2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
addsub_mult_accum2default:default2!
POUT[fab][57]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
addsub_mult_accum2default:default2!
POUT[fab][56]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
addsub_mult_accum2default:default2!
POUT[fab][55]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
addsub_mult_accum2default:default2!
POUT[fab][54]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
addsub_mult_accum2default:default2!
POUT[fab][53]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
addsub_mult_accum2default:default2!
POUT[fab][52]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
addsub_mult_accum2default:default2!
POUT[fab][51]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
addsub_mult_accum2default:default2!
POUT[fab][50]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
addsub_mult_accum2default:default2!
POUT[fab][49]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
addsub_mult_accum2default:default2!
POUT[fab][48]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
addsub_mult_accum2default:default2"
POUT[casc][62]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
addsub_mult_accum2default:default2"
POUT[casc][61]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
addsub_mult_accum2default:default2"
POUT[casc][60]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
addsub_mult_accum2default:default2"
POUT[casc][59]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
addsub_mult_accum2default:default2"
POUT[casc][58]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
addsub_mult_accum2default:default2"
POUT[casc][57]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
addsub_mult_accum2default:default2"
POUT[casc][56]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
addsub_mult_accum2default:default2"
POUT[casc][55]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
addsub_mult_accum2default:default2"
POUT[casc][54]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
addsub_mult_accum2default:default2"
POUT[casc][53]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
addsub_mult_accum2default:default2"
POUT[casc][52]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
addsub_mult_accum2default:default2"
POUT[casc][51]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
addsub_mult_accum2default:default2"
POUT[casc][50]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
addsub_mult_accum2default:default2"
POUT[casc][49]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
addsub_mult_accum2default:default2"
POUT[casc][48]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
addsub_mult_accum2default:default2 
PIN[fab][57]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
addsub_mult_accum2default:default2 
PIN[fab][56]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
addsub_mult_accum2default:default2 
PIN[fab][55]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
addsub_mult_accum2default:default2 
PIN[fab][54]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
addsub_mult_accum2default:default2 
PIN[fab][53]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
addsub_mult_accum2default:default2 
PIN[fab][52]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
addsub_mult_accum2default:default2 
PIN[fab][51]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
addsub_mult_accum2default:default2 
PIN[fab][50]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
addsub_mult_accum2default:default2 
PIN[fab][49]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
addsub_mult_accum2default:default2 
PIN[fab][48]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
addsub_mult_accum2default:default2 
PIN[fab][47]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
addsub_mult_accum2default:default2 
PIN[fab][46]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
addsub_mult_accum2default:default2 
PIN[fab][45]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
addsub_mult_accum2default:default2 
PIN[fab][44]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
addsub_mult_accum2default:default2 
PIN[fab][43]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
addsub_mult_accum2default:default2 
PIN[fab][42]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
addsub_mult_accum2default:default2 
PIN[fab][41]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
addsub_mult_accum2default:default2 
PIN[fab][40]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
addsub_mult_accum2default:default2 
PIN[fab][39]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
addsub_mult_accum2default:default2 
PIN[fab][38]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
addsub_mult_accum2default:default2 
PIN[fab][37]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
addsub_mult_accum2default:default2 
PIN[fab][36]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
addsub_mult_accum2default:default2 
PIN[fab][35]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
addsub_mult_accum2default:default2 
PIN[fab][34]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
addsub_mult_accum2default:default2 
PIN[fab][33]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
addsub_mult_accum2default:default2 
PIN[fab][32]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
addsub_mult_accum2default:default2 
PIN[fab][31]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
addsub_mult_accum2default:default2 
PIN[fab][30]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
addsub_mult_accum2default:default2 
PIN[fab][29]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
addsub_mult_accum2default:default2 
PIN[fab][28]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
addsub_mult_accum2default:default2 
PIN[fab][27]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
addsub_mult_accum2default:default2 
PIN[fab][26]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
addsub_mult_accum2default:default2 
PIN[fab][25]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
addsub_mult_accum2default:default2 
PIN[fab][24]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
addsub_mult_accum2default:default2 
PIN[fab][23]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
addsub_mult_accum2default:default2 
PIN[fab][22]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
addsub_mult_accum2default:default2 
PIN[fab][21]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
addsub_mult_accum2default:default2 
PIN[fab][20]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
addsub_mult_accum2default:default2 
PIN[fab][19]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
addsub_mult_accum2default:default2 
PIN[fab][18]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
addsub_mult_accum2default:default2 
PIN[fab][17]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
addsub_mult_accum2default:default2 
PIN[fab][16]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
addsub_mult_accum2default:default2 
PIN[fab][15]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
addsub_mult_accum2default:default2 
PIN[fab][14]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
addsub_mult_accum2default:default2 
PIN[fab][13]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
addsub_mult_accum2default:default2 
PIN[fab][12]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
addsub_mult_accum2default:default2 
PIN[fab][11]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
addsub_mult_accum2default:default2 
PIN[fab][10]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
addsub_mult_accum2default:default2
PIN[fab][9]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
addsub_mult_accum2default:default2
PIN[fab][8]2default:defaultZ8-3331h px� 
�
!design %s has unconnected port %s3331*oasys2%
addsub_mult_accum2default:default2
PIN[fab][7]2default:defaultZ8-3331h px� 
�
�Message '%s' appears more than %s times and has been disabled. User can change this message limit to see more message instances.
14*common2 
Synth 8-33312default:default2
1002default:defaultZ17-14h px� 
�
%s*synth2�
�Finished RTL Elaboration : Time (s): cpu = 00:00:23 ; elapsed = 00:00:33 . Memory (MB): peak = 1933.164 ; gain = 281.402 ; free physical = 2238 ; free virtual = 8004
2default:defaulth px� 
D
%s
*synth2,

Report Check Netlist: 
2default:defaulth p
x
� 
u
%s
*synth2]
I+------+------------------+-------+---------+-------+------------------+
2default:defaulth p
x
� 
u
%s
*synth2]
I|      |Item              |Errors |Warnings |Status |Description       |
2default:defaulth p
x
� 
u
%s
*synth2]
I+------+------------------+-------+---------+-------+------------------+
2default:defaulth p
x
� 
u
%s
*synth2]
I|1     |multi_driven_nets |      0|        0|Passed |Multi driven nets |
2default:defaulth p
x
� 
u
%s
*synth2]
I+------+------------------+-------+---------+-------+------------------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
M
%s
*synth25
!Start Handling Custom Attributes
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Handling Custom Attributes : Time (s): cpu = 00:00:24 ; elapsed = 00:00:34 . Memory (MB): peak = 1933.164 ; gain = 281.402 ; free physical = 2203 ; free virtual = 7969
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished RTL Optimization Phase 1 : Time (s): cpu = 00:00:24 ; elapsed = 00:00:34 . Memory (MB): peak = 1933.164 ; gain = 281.402 ; free physical = 2203 ; free virtual = 7969
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
e
-Analyzing %s Unisim elements for replacement
17*netlist2
12default:defaultZ29-17h px� 
j
2Unisim Transformation completed in %s CPU seconds
28*netlist2
02default:defaultZ29-28h px� 
K
)Preparing netlist for logic optimization
349*projectZ1-570h px� 
>

Processing XDC Constraints
244*projectZ1-262h px� 
=
Initializing timing engine
348*projectZ1-569h px� 
�
$Parsing XDC File [%s] for cell '%s'
848*designutils2�
�/home/gderiu/AES2021/audio_lab/audio_lab/audio_lab.srcs/sources_1/bd/design_1/ip/design_1_fir_compiler_0_0/design_1_fir_compiler_0_0_ooc.xdc2default:default2
U0	2default:default8Z20-848h px� 
�
-Finished Parsing XDC File [%s] for cell '%s'
847*designutils2�
�/home/gderiu/AES2021/audio_lab/audio_lab/audio_lab.srcs/sources_1/bd/design_1/ip/design_1_fir_compiler_0_0/design_1_fir_compiler_0_0_ooc.xdc2default:default2
U0	2default:default8Z20-847h px� 
�
$Parsing XDC File [%s] for cell '%s'
848*designutils2�
�/home/gderiu/AES2021/audio_lab/audio_lab/audio_lab.srcs/sources_1/bd/design_1/ip/design_1_fir_compiler_0_0/constraints/fir_compiler_v7_2.xdc2default:default2
U0	2default:default8Z20-848h px� 
�
-Finished Parsing XDC File [%s] for cell '%s'
847*designutils2�
�/home/gderiu/AES2021/audio_lab/audio_lab/audio_lab.srcs/sources_1/bd/design_1/ip/design_1_fir_compiler_0_0/constraints/fir_compiler_v7_2.xdc2default:default2
U0	2default:default8Z20-847h px� 
�
Parsing XDC File [%s]
179*designutils2~
h/home/gderiu/AES2021/audio_lab/audio_lab/audio_lab.runs/design_1_fir_compiler_0_0_synth_1/dont_touch.xdc2default:default8Z20-179h px� 
�
Finished Parsing XDC File [%s]
178*designutils2~
h/home/gderiu/AES2021/audio_lab/audio_lab/audio_lab.runs/design_1_fir_compiler_0_0_synth_1/dont_touch.xdc2default:default8Z20-178h px� 
H
&Completed Processing XDC Constraints

245*projectZ1-263h px� 
�
r%sTime (s): cpu = %s ; elapsed = %s . Memory (MB): peak = %s ; gain = %s ; free physical = %s ; free virtual = %s
480*common2.
Netlist sorting complete. 2default:default2
00:00:002default:default2
00:00:002default:default2
2022.9772default:default2
0.0002default:default2
20632default:default2
78292default:defaultZ17-722h px� 
~
!Unisim Transformation Summary:
%s111*project29
%No Unisim elements were transformed.
2default:defaultZ1-111h px� 
�
r%sTime (s): cpu = %s ; elapsed = %s . Memory (MB): peak = %s ; gain = %s ; free physical = %s ; free virtual = %s
480*common24
 Constraint Validation Runtime : 2default:default2
00:00:00.112default:default2
00:00:00.102default:default2
2022.9772default:default2
0.0002default:default2
20592default:default2
78252default:defaultZ17-722h px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Constraint Validation : Time (s): cpu = 00:00:33 ; elapsed = 00:00:46 . Memory (MB): peak = 2022.977 ; gain = 371.215 ; free physical = 2818 ; free virtual = 8591
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
V
%s
*synth2>
*Start Loading Part and Timing Information
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
J
%s
*synth22
Loading part: xc7z010clg400-1
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Loading Part and Timing Information : Time (s): cpu = 00:00:33 ; elapsed = 00:00:46 . Memory (MB): peak = 2022.977 ; gain = 371.215 ; free physical = 2818 ; free virtual = 8591
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
Z
%s
*synth2B
.Start Applying 'set_property' XDC Constraints
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished applying 'set_property' XDC Constraints : Time (s): cpu = 00:00:33 ; elapsed = 00:00:46 . Memory (MB): peak = 2022.977 ; gain = 371.215 ; free physical = 2820 ; free virtual = 8593
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
fAttribute ram_style/rom_style = distributed specified for ROM  "%s". This will be implemented in logic3994*oasys2
rom2default:defaultZ8-5542h px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished RTL Optimization Phase 2 : Time (s): cpu = 00:00:35 ; elapsed = 00:00:48 . Memory (MB): peak = 2022.977 ; gain = 371.215 ; free physical = 2807 ; free virtual = 8582
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
$decloning instance '%s' (%s) to '%s'223*oasys2n
ZU0/i_synth/g_single_rate.i_single_rate/g_semi_parallel_and_smac.g_cntrl_signals[4].i_delay2default:default2*
delay__parameterized102default:default2n
ZU0/i_synth/g_single_rate.i_single_rate/g_semi_parallel_and_smac.g_cntrl_signals[5].i_delay2default:defaultZ8-223h px� 
E
%s
*synth2-

Report RTL Partitions: 
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
+| |RTL Partition |Replication |Instances |
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
L
%s
*synth24
 Start RTL Component Statistics 
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
O
%s
*synth27
#Finished RTL Component Statistics 
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
Y
%s
*synth2A
-Start RTL Hierarchical Component Statistics 
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
[
%s
*synth2C
/Finished RTL Hierarchical Component Statistics
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
H
%s
*synth20
Start Part Resource Summary
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s
*synth2j
VPart Resources:
DSPs: 80 (col length:40)
BRAMs: 120 (col length: RAMB18 40 RAMB36 20)
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
K
%s
*synth23
Finished Part Resource Summary
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
W
%s
*synth2?
+Start Cross Boundary and Area Optimization
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
]
%s
*synth2E
1Warning: Parallel synthesis criteria is not met 
2default:defaulth p
x
� 
�
fAttribute ram_style/rom_style = distributed specified for ROM  "%s". This will be implemented in logic3994*oasys2
p_0_out2default:defaultZ8-5542h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2f
RU0/i_synth/g_single_rate.i_single_rate/g_semi_parallel_and_smac.we_gen_algn_reg[2]2default:default2
FD2default:default2d
PU0/i_synth/g_single_rate.i_single_rate/g_semi_parallel_and_smac.we_gen_cntrl_reg2default:defaultZ8-3886h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2�
nU0/i_synth/p_0_out_inferred__0/\g_single_rate.i_single_rate/g_semi_parallel_and_smac.gen_data_sym_addr_reg[0] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2{
gU0/i_synth/p_0_out_inferred/\g_single_rate.i_single_rate/g_semi_parallel_and_smac.gen_data_addr_reg[0] 2default:defaultZ8-3333h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2t
`U0/i_synth/g_single_rate.i_single_rate/g_semi_parallel_and_smac.g_smac_cntrl.accum_opcode_reg[3]2default:default2
FD2default:default2t
`U0/i_synth/g_single_rate.i_single_rate/g_semi_parallel_and_smac.g_smac_cntrl.accum_opcode_reg[5]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2t
`U0/i_synth/g_single_rate.i_single_rate/g_semi_parallel_and_smac.g_smac_cntrl.accum_opcode_reg[6]2default:default2
FD2default:default2t
`U0/i_synth/g_single_rate.i_single_rate/g_semi_parallel_and_smac.g_smac_cntrl.accum_opcode_reg[9]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2t
`U0/i_synth/g_single_rate.i_single_rate/g_semi_parallel_and_smac.g_smac_cntrl.accum_opcode_reg[7]2default:default2
FD2default:default2t
`U0/i_synth/g_single_rate.i_single_rate/g_semi_parallel_and_smac.g_smac_cntrl.accum_opcode_reg[9]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2t
`U0/i_synth/g_single_rate.i_single_rate/g_semi_parallel_and_smac.g_smac_cntrl.accum_opcode_reg[9]2default:default2
FD2default:default2t
`U0/i_synth/g_single_rate.i_single_rate/g_semi_parallel_and_smac.g_smac_cntrl.accum_opcode_reg[2]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2t
`U0/i_synth/g_single_rate.i_single_rate/g_semi_parallel_and_smac.g_smac_cntrl.accum_opcode_reg[0]2default:default2
FD2default:default2t
`U0/i_synth/g_single_rate.i_single_rate/g_semi_parallel_and_smac.g_smac_cntrl.accum_opcode_reg[2]2default:defaultZ8-3886h px� 
�
"merging instance '%s' (%s) to '%s'3436*oasys2t
`U0/i_synth/g_single_rate.i_single_rate/g_semi_parallel_and_smac.g_smac_cntrl.accum_opcode_reg[1]2default:default2
FD2default:default2t
`U0/i_synth/g_single_rate.i_single_rate/g_semi_parallel_and_smac.g_smac_cntrl.accum_opcode_reg[2]2default:defaultZ8-3886h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2v
bU0/i_synth/\g_single_rate.i_single_rate/g_semi_parallel_and_smac.g_smac_cntrl.accum_opcode_reg[2] 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2�
~U0/i_synth/\g_single_rate.i_single_rate/g_semi_parallel_and_smac.g_cnfg_and_reload.i_cnfg_and_reload/CONFIG_TLAST_MISSING_reg 2default:defaultZ8-3333h px� 
�
6propagating constant %s across sequential element (%s)3333*oasys2
02default:default2l
XU0/i_synth/\g_single_rate.i_single_rate/g_m_data_chan_no_fifo.m_axis_data_tlast_int_reg 2default:defaultZ8-3333h px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Cross Boundary and Area Optimization : Time (s): cpu = 00:00:36 ; elapsed = 00:00:50 . Memory (MB): peak = 2022.977 ; gain = 371.215 ; free physical = 2773 ; free virtual = 8553
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
E
%s
*synth2-

Report RTL Partitions: 
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
+| |RTL Partition |Replication |Instances |
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
R
%s
*synth2:
&Start Applying XDC Timing Constraints
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Applying XDC Timing Constraints : Time (s): cpu = 00:00:51 ; elapsed = 00:01:04 . Memory (MB): peak = 2022.977 ; gain = 371.215 ; free physical = 2473 ; free virtual = 8247
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
F
%s
*synth2.
Start Timing Optimization
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Timing Optimization : Time (s): cpu = 00:00:51 ; elapsed = 00:01:05 . Memory (MB): peak = 2022.977 ; gain = 371.215 ; free physical = 2465 ; free virtual = 8237
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
E
%s
*synth2-

Report RTL Partitions: 
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
+| |RTL Partition |Replication |Instances |
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
E
%s
*synth2-
Start Technology Mapping
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Technology Mapping : Time (s): cpu = 00:00:51 ; elapsed = 00:01:05 . Memory (MB): peak = 2022.977 ; gain = 371.215 ; free physical = 2466 ; free virtual = 8238
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
E
%s
*synth2-

Report RTL Partitions: 
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
+| |RTL Partition |Replication |Instances |
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
?
%s
*synth2'
Start IO Insertion
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
Q
%s
*synth29
%Start Flattening Before IO Insertion
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
T
%s
*synth2<
(Finished Flattening Before IO Insertion
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
H
%s
*synth20
Start Final Netlist Cleanup
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
K
%s
*synth23
Finished Final Netlist Cleanup
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished IO Insertion : Time (s): cpu = 00:00:54 ; elapsed = 00:01:08 . Memory (MB): peak = 2022.977 ; gain = 371.215 ; free physical = 2474 ; free virtual = 8247
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
D
%s
*synth2,

Report Check Netlist: 
2default:defaulth p
x
� 
u
%s
*synth2]
I+------+------------------+-------+---------+-------+------------------+
2default:defaulth p
x
� 
u
%s
*synth2]
I|      |Item              |Errors |Warnings |Status |Description       |
2default:defaulth p
x
� 
u
%s
*synth2]
I+------+------------------+-------+---------+-------+------------------+
2default:defaulth p
x
� 
u
%s
*synth2]
I|1     |multi_driven_nets |      0|        0|Passed |Multi driven nets |
2default:defaulth p
x
� 
u
%s
*synth2]
I+------+------------------+-------+---------+-------+------------------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
O
%s
*synth27
#Start Renaming Generated Instances
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Renaming Generated Instances : Time (s): cpu = 00:00:54 ; elapsed = 00:01:08 . Memory (MB): peak = 2022.977 ; gain = 371.215 ; free physical = 2473 ; free virtual = 8246
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
E
%s
*synth2-

Report RTL Partitions: 
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
+| |RTL Partition |Replication |Instances |
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
W
%s
*synth2?
++-+--------------+------------+----------+
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
L
%s
*synth24
 Start Rebuilding User Hierarchy
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Rebuilding User Hierarchy : Time (s): cpu = 00:00:54 ; elapsed = 00:01:08 . Memory (MB): peak = 2022.977 ; gain = 371.215 ; free physical = 2464 ; free virtual = 8238
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
K
%s
*synth23
Start Renaming Generated Ports
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Renaming Generated Ports : Time (s): cpu = 00:00:54 ; elapsed = 00:01:08 . Memory (MB): peak = 2022.977 ; gain = 371.215 ; free physical = 2464 ; free virtual = 8238
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
M
%s
*synth25
!Start Handling Custom Attributes
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Handling Custom Attributes : Time (s): cpu = 00:00:54 ; elapsed = 00:01:08 . Memory (MB): peak = 2022.977 ; gain = 371.215 ; free physical = 2463 ; free virtual = 8237
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
J
%s
*synth22
Start Renaming Generated Nets
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Renaming Generated Nets : Time (s): cpu = 00:00:54 ; elapsed = 00:01:08 . Memory (MB): peak = 2022.977 ; gain = 371.215 ; free physical = 2463 ; free virtual = 8237
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
K
%s
*synth23
Start Writing Synthesis Report
2default:defaulth p
x
� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
A
%s
*synth2)

Report BlackBoxes: 
2default:defaulth p
x
� 
J
%s
*synth22
+-+--------------+----------+
2default:defaulth p
x
� 
J
%s
*synth22
| |BlackBox name |Instances |
2default:defaulth p
x
� 
J
%s
*synth22
+-+--------------+----------+
2default:defaulth p
x
� 
J
%s
*synth22
+-+--------------+----------+
2default:defaulth p
x
� 
A
%s*synth2)

Report Cell Usage: 
2default:defaulth px� 
E
%s*synth2-
+------+--------+------+
2default:defaulth px� 
E
%s*synth2-
|      |Cell    |Count |
2default:defaulth px� 
E
%s*synth2-
+------+--------+------+
2default:defaulth px� 
E
%s*synth2-
|1     |DSP48E1 |     1|
2default:defaulth px� 
E
%s*synth2-
|2     |LUT2    |    13|
2default:defaulth px� 
E
%s*synth2-
|3     |LUT3    |     8|
2default:defaulth px� 
E
%s*synth2-
|4     |LUT4    |    16|
2default:defaulth px� 
E
%s*synth2-
|5     |LUT5    |     6|
2default:defaulth px� 
E
%s*synth2-
|6     |LUT6    |     9|
2default:defaulth px� 
E
%s*synth2-
|7     |SRL16E  |    56|
2default:defaulth px� 
E
%s*synth2-
|8     |SRLC32E |    32|
2default:defaulth px� 
E
%s*synth2-
|9     |FDRE    |   185|
2default:defaulth px� 
E
%s*synth2-
|10    |FDSE    |     2|
2default:defaulth px� 
E
%s*synth2-
+------+--------+------+
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
�
%s*synth2�
�Finished Writing Synthesis Report : Time (s): cpu = 00:00:54 ; elapsed = 00:01:08 . Memory (MB): peak = 2022.977 ; gain = 371.215 ; free physical = 2463 ; free virtual = 8237
2default:defaulth px� 
~
%s
*synth2f
R---------------------------------------------------------------------------------
2default:defaulth p
x
� 
s
%s
*synth2[
GSynthesis finished with 0 errors, 0 critical warnings and 29 warnings.
2default:defaulth p
x
� 
�
%s
*synth2�
�Synthesis Optimization Runtime : Time (s): cpu = 00:00:50 ; elapsed = 00:01:03 . Memory (MB): peak = 2022.977 ; gain = 281.402 ; free physical = 2494 ; free virtual = 8269
2default:defaulth p
x
� 
�
%s
*synth2�
�Synthesis Optimization Complete : Time (s): cpu = 00:00:54 ; elapsed = 00:01:08 . Memory (MB): peak = 2022.977 ; gain = 371.215 ; free physical = 2494 ; free virtual = 8268
2default:defaulth p
x
� 
B
 Translating synthesized netlist
350*projectZ1-571h px� 
e
-Analyzing %s Unisim elements for replacement
17*netlist2
12default:defaultZ29-17h px� 
j
2Unisim Transformation completed in %s CPU seconds
28*netlist2
02default:defaultZ29-28h px� 
K
)Preparing netlist for logic optimization
349*projectZ1-570h px� 
u
)Pushed %s inverter(s) to %s load pin(s).
98*opt2
02default:default2
02default:defaultZ31-138h px� 
�
r%sTime (s): cpu = %s ; elapsed = %s . Memory (MB): peak = %s ; gain = %s ; free physical = %s ; free virtual = %s
480*common2.
Netlist sorting complete. 2default:default2
00:00:002default:default2
00:00:002default:default2
2055.8832default:default2
0.0002default:default2
24062default:default2
81802default:defaultZ17-722h px� 
~
!Unisim Transformation Summary:
%s111*project29
%No Unisim elements were transformed.
2default:defaultZ1-111h px� 
U
Releasing license: %s
83*common2
	Synthesis2default:defaultZ17-83h px� 
�
G%s Infos, %s Warnings, %s Critical Warnings and %s Errors encountered.
28*	vivadotcl2
352default:default2
1012default:default2
02default:default2
02default:defaultZ4-41h px� 
^
%s completed successfully
29*	vivadotcl2 
synth_design2default:defaultZ4-42h px� 
�
r%sTime (s): cpu = %s ; elapsed = %s . Memory (MB): peak = %s ; gain = %s ; free physical = %s ; free virtual = %s
480*common2"
synth_design: 2default:default2
00:01:032default:default2
00:01:152default:default2
2055.8832default:default2
606.3362default:default2
25002default:default2
82742default:defaultZ17-722h px� 
�
r%sTime (s): cpu = %s ; elapsed = %s . Memory (MB): peak = %s ; gain = %s ; free physical = %s ; free virtual = %s
480*common2.
Netlist sorting complete. 2default:default2
00:00:002default:default2
00:00:002default:default2
2055.8832default:default2
0.0002default:default2
25042default:default2
82782default:defaultZ17-722h px� 
K
"No constraints selected for write.1103*constraintsZ18-5210h px� 
�
 The %s '%s' has been generated.
621*common2

checkpoint2default:default2�
w/home/gderiu/AES2021/audio_lab/audio_lab/audio_lab.runs/design_1_fir_compiler_0_0_synth_1/design_1_fir_compiler_0_0.dcp2default:defaultZ17-1381h px� 
�
'%s' is deprecated. %s
384*common2#
use_project_ipc2default:default2A
-This option is deprecated and no longer used.2default:defaultZ17-576h px� 
�
<Added synthesis output to IP cache for IP %s, cache-ID = %s
485*coretcl2-
design_1_fir_compiler_0_02default:default2$
66123bd585f87f252default:defaultZ2-1648h px� 
Q
Renamed %s cell refs.
330*coretcl2
242default:defaultZ2-1174h px� 
�
r%sTime (s): cpu = %s ; elapsed = %s . Memory (MB): peak = %s ; gain = %s ; free physical = %s ; free virtual = %s
480*common2.
Netlist sorting complete. 2default:default2
00:00:002default:default2
00:00:002default:default2
2079.8952default:default2
0.0002default:default2
24212default:default2
82012default:defaultZ17-722h px� 
K
"No constraints selected for write.1103*constraintsZ18-5210h px� 
�
 The %s '%s' has been generated.
621*common2

checkpoint2default:default2�
w/home/gderiu/AES2021/audio_lab/audio_lab/audio_lab.runs/design_1_fir_compiler_0_0_synth_1/design_1_fir_compiler_0_0.dcp2default:defaultZ17-1381h px� 
�
%s4*runtcl2�
�Executing : report_utilization -file design_1_fir_compiler_0_0_utilization_synth.rpt -pb design_1_fir_compiler_0_0_utilization_synth.pb
2default:defaulth px� 
�
Exiting %s at %s...
206*common2
Vivado2default:default2,
Fri Nov 12 18:55:34 20212default:defaultZ17-206h px� 


End Record