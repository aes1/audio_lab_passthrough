/******************************************************************************
*
* Copyright (C) 2009 - 2014 Xilinx, Inc.  All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* Use of the Software is limited solely to applications:
* (a) running on a Xilinx device, or
* (b) that interact with a Xilinx device through a bus or interconnect.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
* XILINX  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
* OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*
* Except as contained in this notice, the name of the Xilinx shall not be used
* in advertising or otherwise to promote the sale, use or other dealings in
* this Software without prior written authorization from Xilinx.
*
******************************************************************************/

/*
 * helloworld.c: simple test application
 *
 * This application configures UART 16550 to baud rate 9600.
 * PS7 UART (Zynq) is not initialized by this application, since
 * bootrom/bsp configures it to baud rate 115200
 *
 * ------------------------------------------------
 * | UART TYPE   BAUD RATE                        |
 * ------------------------------------------------
 *   uartns550   9600
 *   uartlite    Configurable only in HW design
 *   ps7_uart    115200 (configured by bootrom/bsp)
 */

#include <stdio.h>
#include "platform.h"
#include "xil_printf.h"
#include "xil_io.h"
#include "xiicps.h"
#include "timer_ps.h"
#include <time.h>
#include <stdlib.h>
#include <math.h>
/* I2S Register offsets */
#define I2S_RESET_REG 		0x00
#define I2S_CTRL_REG 		0x04
#define I2S_CLK_CTRL_REG 	0x08
#define I2S_FIFO_STS_REG 	0x20
#define I2S_RX_FIFO_REG 	0x28
#define I2S_TX_FIFO_REG 	0x2C

#define FIFO_ISR (XPAR_AXI_FIFO_MM_S_0_BASEADDR + 0x00)
#define FIFO_IER (XPAR_AXI_FIFO_MM_S_0_BASEADDR + 0x04)
#define FIFO_TDFV (XPAR_AXI_FIFO_MM_S_0_BASEADDR + 0x0C)
#define FIFO_RDFO (XPAR_AXI_FIFO_MM_S_0_BASEADDR + 0x1C)
#define FIFO_TDR (XPAR_AXI_FIFO_MM_S_0_BASEADDR + 0x2C)
#define FIFO_TDFD (XPAR_AXI_FIFO_MM_S_0_BASEADDR + 0x10)
#define FIFO_TLR (XPAR_AXI_FIFO_MM_S_0_BASEADDR + 0x14)


#define FIFO_RLR (XPAR_AXI_FIFO_MM_S_0_BASEADDR + 0x24)
#define FIFO_RDFD (XPAR_AXI_FIFO_MM_S_0_BASEADDR + 0x20)
#define FIFO_RDR (XPAR_AXI_FIFO_MM_S_0_BASEADDR + 0x30)

/* IIC address of the SSM2603 device and the desired IIC clock speed */
#define IIC_SLAVE_ADDR		0b0011010
#define IIC_SCLK_RATE		100000


#define AUDIO_IIC_ID XPAR_XIICPS_0_DEVICE_ID
#define AUDIO_CTRL_BASEADDR XPAR_AXI_I2S_ADI_0_S00_AXI_BASEADDR
#define SCU_TIMER_ID XPAR_SCUTIMER_DEVICE_ID


#define SWI_BASE_ADDR XPAR_AXI_GPIO_2_BASEADDR
#define LED_BASE_ADDR XPAR_AXI_GPIO_1_BASEADDR
#define BUT_BASE_ADDR XPAR_AXI_GPIO_0_BASEADDR

#define AUDIO_FIFO XPAR_AXI_FIFO_MM_S_0_BASEADDR

#define GLOBAL_TMR_BASEADDR XPAR_PS7_GLOBALTIMER_0_S_AXI_BASEADDR
/* ------------------------------------------------------------ */
/*				Low-Pass and High-Pass FIR filter coefficients									*/
/* ------------------------------------------------------------ */

#define coeffLP -1.692219e-02, 5.043750e-02,3.935835e-02,4.341238e-02,5.137933e-02,5.982048e-02,6.748827e-02, 7.379049e-02, 7.824605e-02, 8.052560e-02,8.052560e-02, 7.824605e-02, 7.379049e-02, 6.748827e-02,5.982048e-02,5.137933e-02,4.341238e-02,3.935835e-02,5.043750e-02,-0.0169221860
#define coeffHP -3.942071e-02,-5.929114e-03,1.430997e-02,4.069053e-02,5.762197e-02,4.843584e-02,4.349633e-03,-6.932913e-02,-1.527862e-01,-2.187328e-01,7.562085e-01,-2.187328e-01,-1.527862e-01,-6.932913e-02,4.349633e-03,4.843584e-02,5.762197e-02,4.069053e-02,1.430997e-02,-5.929114e-03,-0.0394207089
#define N_LP 20 // ordine filtro
#define N_HP 21 // ordine filtro

float LP[]={coeffLP};
float HP[]={coeffHP};


/* ------------------------------------------------------------ */
/*				Global Variables								*/
/* ------------------------------------------------------------ */

XIicPs Iic;		/* Instance of the IIC Device */

/* ------------------------------------------------------------ */
/*				Procedure Definitions							*/
/* ------------------------------------------------------------ */

int AudioRegSet(XIicPs *IIcPtr, u8 regAddr, u16 regData)
{
	int Status;
	u8 SendBuffer[2];

	SendBuffer[0] = regAddr << 1;
	SendBuffer[0] = SendBuffer[0] | ((regData >> 8) & 0b1);

	SendBuffer[1] = regData & 0xFF;

	Status = XIicPs_MasterSendPolled(IIcPtr, SendBuffer,
				 2, IIC_SLAVE_ADDR);
	if (Status != XST_SUCCESS) {
		xil_printf("IIC send failed\n\r");
		return XST_FAILURE;
	}
	/*
	 * Wait until bus is idle to start another transfer.
	 */
	while (XIicPs_BusIsBusy(IIcPtr)) {
		/* NOP */
	}
	return XST_SUCCESS;

}
/***	AudioInitialize(u16 timerID,  u16 iicID, u32 i2sAddr)
**
**	Parameters:
**		timerID - DEVICE_ID for the SCU timer
**		iicID 	- DEVICE_ID for the PS IIC controller connected to the SSM2603
**		i2sAddr - Physical Base address of the I2S controller
**
**	Return Value: int
**		XST_SUCCESS if successful
**
**	Errors:
**
**	Description:
**		Initializes the Audio demo. Must be called once and only once before calling
**		AudioRunDemo
**
*/
int AudioInitialize(u16 timerID,  u16 iicID, u32 i2sAddr) //, u32 i2sTransmAddr, u32 i2sReceivAddr)
{
	int Status;
	XIicPs_Config *Config;
	u32 i2sClkDiv;

	TimerInitialize(timerID);

	/*
	 * Initialize the IIC driver so that it's ready to use
	 * Look up the configuration in the config table,
	 * then initialize it.
	 */
	Config = XIicPs_LookupConfig(iicID);
	if (NULL == Config) {
		return XST_FAILURE;
	}

	Status = XIicPs_CfgInitialize(&Iic, Config, Config->BaseAddress);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	/*
	 * Perform a self-test to ensure that the hardware was built correctly.
	 */
	Status = XIicPs_SelfTest(&Iic);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	/*
	 * Set the IIC serial clock rate.
	 */
	Status = XIicPs_SetSClk(&Iic, IIC_SCLK_RATE);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}


	/*
	 * Write to the SSM2603 audio codec registers to configure the device. Refer to the
	 * SSM2603 Audio Codec data sheet for information on what these writes do.
	 */
	Status = AudioRegSet(&Iic, 15, 0b000000000); //Perform Reset
	TimerDelay(75000);
	Status |= AudioRegSet(&Iic, 6, 0b000110000); //Power up
	Status |= AudioRegSet(&Iic, 0, 0b000010111);
	Status |= AudioRegSet(&Iic, 1, 0b000010111);
	Status |= AudioRegSet(&Iic, 2, 0b101111001);
	Status |= AudioRegSet(&Iic, 4, 0b000010000);
	Status |= AudioRegSet(&Iic, 5, 0b000000000);
	Status |= AudioRegSet(&Iic, 7, 0b000001010); //Changed so Word length is 24
	Status |= AudioRegSet(&Iic, 8, 0b000000000); //Changed so no CLKDIV2
	TimerDelay(75000);
	Status |= AudioRegSet(&Iic, 9, 0b000000001);
	Status |= AudioRegSet(&Iic, 6, 0b000100000);
	Status = AudioRegSet(&Iic, 4, 0b000010000);

	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	i2sClkDiv = 1; //Set the BCLK to be MCLK / 4
	i2sClkDiv = i2sClkDiv | (31 << 16); //Set the LRCLK's to be BCLK / 64

	Xil_Out32(i2sAddr + I2S_CLK_CTRL_REG, i2sClkDiv); //Write clock div register

	return XST_SUCCESS;
}

void I2SFifoWrite (u32 i2sBaseAddr, u32 audioData)
{

	Xil_Out32(i2sBaseAddr + 0x10, audioData); // write DATA
    Xil_Out32(i2sBaseAddr + 0x14, 4);    // write the length of the DATA (4 bytes)

	//xil_printf("%x\n", Xil_In32(i2sBaseAddr + 0x00));
	while ((Xil_In32(i2sBaseAddr + 0x00)&0x08000000)!=0x08000000){;}  // waits for the transmission completes
	Xil_Out32(i2sBaseAddr + 0x00, 0x08000000);  // ack the transmission complete


}

u32 I2SFifoRead (u32 i2sBaseAddr)
{

	while (Xil_In32(i2sBaseAddr + 0x1C)==0){;} // waits for a sample in the FIFO
	int data = Xil_In32(i2sBaseAddr + 0x20);   // read the sample from the FIFO
return data;

}

int main()
{

    init_platform();


	AudioInitialize(SCU_TIMER_ID, AUDIO_IIC_ID, AUDIO_CTRL_BASEADDR);

	Xil_Out32(AUDIO_CTRL_BASEADDR + I2S_RESET_REG, 0b110); //Reset RX and TX FIFOs
	Xil_Out32(AUDIO_CTRL_BASEADDR + I2S_CTRL_REG, 0b011); //Enable RX Fifo and TX FIFOs, disable mute

    Xil_Out32(AUDIO_FIFO + 0x2c, 0);

    // init
    print("Started!\n\r");

    xil_printf("FIFO_ISR:  0x%08x\n",Xil_In32(FIFO_ISR));
    print("write FIFO_ISR\n\r");
    Xil_Out32(FIFO_ISR, 0xFFFFFFFF);
    xil_printf("FIFO_ISR:  0x%08x\n",Xil_In32(FIFO_ISR));
    xil_printf("FIFO_IER:  0x%08x\n",Xil_In32(FIFO_IER));
    xil_printf("FIFO_TDFV: 0x%08x\n",Xil_In32(FIFO_TDFV));
    xil_printf("FIFO_RDFO: 0x%08x\n",Xil_In32(FIFO_RDFO));

    print("Write IER\n\r");
    Xil_Out32(FIFO_IER, 0x0C000000);

    print("Write TDR\n\r");
    Xil_Out32(FIFO_TDR, 0x00000000);




    print("Hello World\n\r");
    xil_printf("FIFO_ISR:  0x%08x\n",Xil_In32(FIFO_ISR));
	print("write FIFO_ISR\n\r");
	Xil_Out32(FIFO_ISR, 0xFFFFFFFF);
	xil_printf("FIFO_ISR:  0x%08x\n",Xil_In32(FIFO_ISR));
	xil_printf("FIFO_IER:  0x%08x\n",Xil_In32(FIFO_IER));
	xil_printf("FIFO_TDFV: 0x%08x\n",Xil_In32(FIFO_TDFV));
	xil_printf("FIFO_RDFO: 0x%08x\n",Xil_In32(FIFO_RDFO));


    print("write FIFO_IER\n");
    Xil_Out32(FIFO_IER, 0x04100000);
    xil_printf("FIFO_ISR:  0x%08x\n",Xil_In32(FIFO_ISR));
    print("write FIFO_ISR\n");
    Xil_Out32(FIFO_ISR, 0x00100000);
    int SampleL, SampleR;



	int bufferL[N_HP], bufferR[N_HP];

	int j=0;

	int tempo=0, tempo_old=0;


    while (1){

		tempo_old=tempo;
		tempo = Xil_In32((u32)GLOBAL_TMR_BASEADDR);

		SampleL = (int) I2SFifoRead(AUDIO_FIFO);
		SampleR = (int) I2SFifoRead(AUDIO_FIFO);

		if (Xil_In32(SWI_BASE_ADDR) == 0b0001){ // only left channel
			I2SFifoWrite(AUDIO_FIFO, SampleL);
			I2SFifoWrite(AUDIO_FIFO, 0);
			Xil_Out32(LED_BASE_ADDR, 0b0001);
		}
		else if (Xil_In32(SWI_BASE_ADDR) == 0b0010){// only right channel
			I2SFifoWrite(AUDIO_FIFO, 0);
			I2SFifoWrite(AUDIO_FIFO, SampleR);
			Xil_Out32(LED_BASE_ADDR, 0b0010);
			}


		else if ((Xil_In32(SWI_BASE_ADDR)&0b1000) == 0b1000){ // filter
			j++;




			// applico il filtro sui valori del buffer in funzione del valore dello switch


			if (Xil_In32(SWI_BASE_ADDR)==0b1001) { // apply SW low-pass filter

				float filteredR=0, filteredL=0;

				int i;

				// un singolo for che gestisce due compiti: aggiornamento del buffer, calcolo del valore filtrato
				for (i=0;i<N_HP-1;i++){
					// il buffer scorre da destra verso sinistra, effettuando in questo modo il ribaltamento della finestra
					bufferL[N_HP-1-1-i]=bufferL[N_HP-1-i];
					bufferR[N_HP-1-1-i]=bufferR[N_HP-1-i];
				}
				// carico il nuovo campione nell'ultima posizione del buffer
				bufferR[N_HP-1]=SampleR;
				bufferL[N_HP-1]=SampleL;




				for (i=0;i<N_LP;i++){
				  filteredR+= (float)bufferR[i]*LP[i];
				  filteredL+= (float)bufferL[i]*LP[i];
				}

				I2SFifoWrite(AUDIO_FIFO, (int)filteredL);
				I2SFifoWrite(AUDIO_FIFO, (int)filteredR);
				Xil_Out32(LED_BASE_ADDR, 0b1001);
			}
			else if (Xil_In32(SWI_BASE_ADDR)==0b1010){ // apply SW high-pass filter
				float filteredR=0, filteredL=0;

				int i;

				// un singolo for che gestisce due compiti: aggiornamento del buffer, calcolo del valore filtrato
				for (i=0;i<N_HP-1;i++){
					// il buffer scorre da destra verso sinistra, effettuando in questo modo il ribaltamento della finestra
					bufferL[N_HP-1-1-i]=bufferL[N_HP-1-i];
					bufferR[N_HP-1-1-i]=bufferR[N_HP-1-i];
				}
				// carico il nuovo campione nell'ultima posizione del buffer
				bufferR[N_HP-1]=SampleR;
				bufferL[N_HP-1]=SampleL;



				for (i=0;i<N_HP;i++){
				  filteredR+= (float)bufferR[i]*HP[i];
				  filteredL+= (float)bufferL[i]*HP[i];
				}
				I2SFifoWrite(AUDIO_FIFO, (int)filteredL);
				I2SFifoWrite(AUDIO_FIFO, (int)filteredR);
				Xil_Out32(LED_BASE_ADDR, 0b1010);
			}
			else if (Xil_In32(SWI_BASE_ADDR)==0b1101){ // apply HW low-pass filter
				float filteredR=0, filteredL=0;

				filteredL = (int) I2SFifoRead(FIR_FIFO);
				//filteredR = (int) I2SFifoRead(FIR_FIFO);

				I2SFifoWrite(AUDIO_FIFO, (int)filteredL);
				I2SFifoWrite(AUDIO_FIFO, (int)filteredL);
				Xil_Out32(LED_BASE_ADDR, 0b1101);
			}
			else if (Xil_In32(SWI_BASE_ADDR)==0b1110){ // apply HW high-pass filter

				float filteredR=0, filteredL=0;


				I2SFifoWrite(AUDIO_FIFO, (int)filteredL);
				I2SFifoWrite(AUDIO_FIFO, (int)filteredR);
				Xil_Out32(LED_BASE_ADDR, 0b1110);
			}
			else { // pass-through
				I2SFifoWrite(AUDIO_FIFO, SampleL);
				I2SFifoWrite(AUDIO_FIFO, SampleR);
				Xil_Out32(LED_BASE_ADDR, 0b0000);
				}



		}


		else { // pass-through
			I2SFifoWrite(AUDIO_FIFO, SampleL);
			I2SFifoWrite(AUDIO_FIFO, SampleR);
			Xil_Out32(LED_BASE_ADDR, 0b0000);
			}


		if ( Xil_In32(BUT_BASE_ADDR)==0b1000){ // con questa combinazione interrompo il while (1)
			break;
		}



    }
	xil_printf("Time: %d\n", tempo-tempo_old);

    cleanup_platform();
    return 0;
}
