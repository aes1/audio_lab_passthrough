/******************************************************************************
*
* Copyright (C) 2009 - 2014 Xilinx, Inc.  All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* Use of the Software is limited solely to applications:
* (a) running on a Xilinx device, or
* (b) that interact with a Xilinx device through a bus or interconnect.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
* XILINX  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
* OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*
* Except as contained in this notice, the name of the Xilinx shall not be used
* in advertising or otherwise to promote the sale, use or other dealings in
* this Software without prior written authorization from Xilinx.
*
******************************************************************************/

/*
 * helloworld.c: simple test application
 *
 * This application configures UART 16550 to baud rate 9600.
 * PS7 UART (Zynq) is not initialized by this application, since
 * bootrom/bsp configures it to baud rate 115200
 *
 * ------------------------------------------------
 * | UART TYPE   BAUD RATE                        |
 * ------------------------------------------------
 *   uartns550   9600
 *   uartlite    Configurable only in HW design
 *   ps7_uart    115200 (configured by bootrom/bsp)
 */

#include <stdio.h>
#include "platform.h"
#include "xil_printf.h"
#include "xil_io.h"
#include "xiicps.h"
#include "timer_ps.h"
#include <time.h>
#include <stdlib.h>
#include <math.h>
/* I2S Register offsets */
#define I2S_VERSION_REG 		0x00
#define I2S_CORE_CONGIG_REG 	0x04
#define I2S_CTRL_REG 			0x08
#define I2S_TIMING_CONTROL_REG 	0x20
#define I2S_RX_FIFO_REG 		0x28
#define I2S_TX_FIFO_REG 		0x2C

#define FIFO_ISR (XPAR_AXI_FIFO_MM_S_0_BASEADDR + 0x00)
#define FIFO_IER (XPAR_AXI_FIFO_MM_S_0_BASEADDR + 0x04)
#define FIFO_TDFV (XPAR_AXI_FIFO_MM_S_0_BASEADDR + 0x0C)
#define FIFO_RDFO (XPAR_AXI_FIFO_MM_S_0_BASEADDR + 0x1C)
#define FIFO_TDR (XPAR_AXI_FIFO_MM_S_0_BASEADDR + 0x2C)
#define FIFO_TDFD (XPAR_AXI_FIFO_MM_S_0_BASEADDR + 0x10)
#define FIFO_TLR (XPAR_AXI_FIFO_MM_S_0_BASEADDR + 0x14)


#define FIFO_RLR (XPAR_AXI_FIFO_MM_S_0_BASEADDR + 0x24)
#define FIFO_RDFD (XPAR_AXI_FIFO_MM_S_0_BASEADDR + 0x20)
#define FIFO_RDR (XPAR_AXI_FIFO_MM_S_0_BASEADDR + 0x30)

/* IIC address of the SSM2603 device and the desired IIC clock speed */
#define IIC_SLAVE_ADDR		0b0011010
#define IIC_SCLK_RATE		100000

/* ------------------------------------------------------------ */
/*				Global Variables								*/
/* ------------------------------------------------------------ */

XIicPs Iic;		/* Instance of the IIC Device */

/* ------------------------------------------------------------ */
/*				Procedure Definitions							*/
/* ------------------------------------------------------------ */

int AudioRegSet(XIicPs *IIcPtr, u8 regAddr, u16 regData)
{
	int Status;
	u8 SendBuffer[2];

	SendBuffer[0] = regAddr << 1;
	SendBuffer[0] = SendBuffer[0] | ((regData >> 8) & 0b1);

	SendBuffer[1] = regData & 0xFF;

	Status = XIicPs_MasterSendPolled(IIcPtr, SendBuffer,
				 2, IIC_SLAVE_ADDR);
	if (Status != XST_SUCCESS) {
		xil_printf("IIC send failed\n\r");
		return XST_FAILURE;
	}
	/*
	 * Wait until bus is idle to start another transfer.
	 */
	while (XIicPs_BusIsBusy(IIcPtr)) {
		/* NOP */
	}
	return XST_SUCCESS;

}
/***	AudioInitialize(u16 timerID,  u16 iicID, u32 i2sAddr)
**
**	Parameters:
**		timerID - DEVICE_ID for the SCU timer
**		iicID 	- DEVICE_ID for the PS IIC controller connected to the SSM2603
**		i2sAddr - Physical Base address of the I2S controller
**
**	Return Value: int
**		XST_SUCCESS if successful
**
**	Errors:
**
**	Description:
**		Initializes the Audio demo. Must be called once and only once before calling
**		AudioRunDemo
**
*/
int AudioInitialize(u16 timerID,  u16 iicID, u32 i2sTransmAddr, u32 i2sReceivAddr)
{
	int Status;
	XIicPs_Config *Config;
	u32 i2sClkDiv;

	TimerInitialize(timerID);

	/*
	 * Initialize the IIC driver so that it's ready to use
	 * Look up the configuration in the config table,
	 * then initialize it.
	 */
	Config = XIicPs_LookupConfig(iicID);
	if (NULL == Config) {
		return XST_FAILURE;
	}

	Status = XIicPs_CfgInitialize(&Iic, Config, Config->BaseAddress);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	/*
	 * Perform a self-test to ensure that the hardware was built correctly.
	 */
	Status = XIicPs_SelfTest(&Iic);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	/*
	 * Set the IIC serial clock rate.
	 */
	Status = XIicPs_SetSClk(&Iic, IIC_SCLK_RATE);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}


	/*
	 * Write to the SSM2603 audio codec registers to configure the device. Refer to the
	 * SSM2603 Audio Codec data sheet for information on what these writes do.
	 */
	Status = AudioRegSet(&Iic, 15, 0b000000000); //Perform Reset
	TimerDelay(75000);
	Status |= AudioRegSet(&Iic, 6, 0b000110000); //Power up
	Status |= AudioRegSet(&Iic, 0, 0b000010111);
	Status |= AudioRegSet(&Iic, 1, 0b000010111);
	Status |= AudioRegSet(&Iic, 2, 0b101111001);
	Status |= AudioRegSet(&Iic, 4, 0b000010000);
	Status |= AudioRegSet(&Iic, 5, 0b000000000);
	Status |= AudioRegSet(&Iic, 7, 0b000001010); //Changed so Word length is 24
	Status |= AudioRegSet(&Iic, 8, 0b000000000); //Changed so no CLKDIV2
	TimerDelay(75000);
	Status |= AudioRegSet(&Iic, 9, 0b000000001);
	Status |= AudioRegSet(&Iic, 6, 0b000100000);
	Status = AudioRegSet(&Iic, 4, 0b000010000);

	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	//i2sClkDiv = 1; //Set the BCLK to be MCLK / 4

	i2sClkDiv = 4;

	Xil_Out32(i2sTransmAddr + I2S_TIMING_CONTROL_REG, i2sClkDiv); //Write clock div register
	Xil_Out32(i2sReceivAddr + I2S_TIMING_CONTROL_REG, i2sClkDiv); //Write clock div register


	return XST_SUCCESS;
}

#define AUDIO_IIC_ID XPAR_XIICPS_0_DEVICE_ID
#define TRANSM_CTRL_BASEADDR XPAR_I2S_TRANSMITTER_0_BASEADDR
#define RECEIV_CTRL_BASEADDR XPAR_I2S_RECEIVER_0_BASEADDR
#define SCU_TIMER_ID XPAR_SCUTIMER_DEVICE_ID

int main()
{

    init_platform();


	AudioInitialize(SCU_TIMER_ID, AUDIO_IIC_ID, TRANSM_CTRL_BASEADDR, RECEIV_CTRL_BASEADDR);

    Xil_Out32(XPAR_I2S_RECEIVER_0_BASEADDR    + I2S_CTRL_REG, 0x00010001);
    Xil_Out32(XPAR_I2S_TRANSMITTER_0_BASEADDR + I2S_CTRL_REG, 1);

    Xil_Out32(XPAR_I2S_TRANSMITTER_0_BASEADDR + 0x0c, 1); //validity

    Xil_Out32(XPAR_AXI_FIFO_MM_S_0_BASEADDR + 0x2c, 0);

    // init
  /*  print("Hello World\n\r");

    xil_printf("FIFO_ISR:  0x%08x\n",Xil_In32(FIFO_ISR));
    print("write FIFO_ISR\n\r");
    Xil_Out32(FIFO_ISR, 0xFFFFFFFF);
    xil_printf("FIFO_ISR:  0x%08x\n",Xil_In32(FIFO_ISR));
    xil_printf("FIFO_IER:  0x%08x\n",Xil_In32(FIFO_IER));
    xil_printf("FIFO_TDFV: 0x%08x\n",Xil_In32(FIFO_TDFV));
    xil_printf("FIFO_RDFO: 0x%08x\n",Xil_In32(FIFO_RDFO));

    print("Write IER\n\r");
    Xil_Out32(FIFO_IER, 0x0C000000);

    print("Write TDR\n\r");
    Xil_Out32(FIFO_TDR, 0x00000002);


    //print("Hello World\n\r");
    //xil_printf ("0x%08x \n", Xil_In32(XPAR_I2S_RECEIVER_0_BASEADDR+I2S_CORE_CONGIG_REG));
   // xil_printf ("0x%08x \n", Xil_In32(XPAR_I2S_TRANSMITTER_0_BASEADDR+I2S_CORE_CONGIG_REG));
    int i=0;
    while (1){


    	int a=(int)(cos((double)i/128*M_PI)*8388608);//rand()%0xFFFFF;
    	//xil_printf("a %d\n", a);
    	//xil_printf("vac %d\n", Xil_In32(XPAR_AXI_FIFO_MM_S_0_BASEADDR+0x0c));
    //	xil_printf("int %d\n", Xil_In32(XPAR_AXI_FIFO_MM_S_0_BASEADDR+0x00));


    	Xil_Out32(FIFO_TDFD, a);
        Xil_Out32(FIFO_TLR, 4);
        while ((Xil_In32(FIFO_ISR)&0x08000000)!=0x08000000){;}
        Xil_Out32(FIFO_ISR, 0xFFFFFFFF);
        xil_printf("FIFO_ISR:  0x%08x\n",Xil_In32(FIFO_ISR));
        xil_printf("FIFO_TDFV: 0x%08x\n",Xil_In32(FIFO_TDFV));


    	//if (Xil_In32(XPAR_AXI_FIFO_MM_S_0_BASEADDR+0x1c)){
    	//	xil_printf ("%x \n", Xil_In32(XPAR_AXI_FIFO_MM_S_0_BASEADDR+0x20));
    	//}



        i++;

    }*/

    print("Hello World\n\r");
    xil_printf("FIFO_ISR:  0x%08x\n",Xil_In32(FIFO_ISR));
	print("write FIFO_ISR\n\r");
	Xil_Out32(FIFO_ISR, 0xFFFFFFFF);
	xil_printf("FIFO_ISR:  0x%08x\n",Xil_In32(FIFO_ISR));
	xil_printf("FIFO_IER:  0x%08x\n",Xil_In32(FIFO_IER));
	xil_printf("FIFO_TDFV: 0x%08x\n",Xil_In32(FIFO_TDFV));
	xil_printf("FIFO_RDFO: 0x%08x\n",Xil_In32(FIFO_RDFO));


    print("write FIFO_IER\n");
    Xil_Out32(FIFO_IER, 0x04100000);
    xil_printf("FIFO_ISR:  0x%08x\n",Xil_In32(FIFO_ISR));
    print("write FIFO_ISR\n");
    Xil_Out32(FIFO_ISR, 0x00100000);

    xil_printf("REC INT:  0x%08x\n",Xil_In32(XPAR_I2S_RECEIVER_0_BASEADDR + 0x10));
    while (1){
    	//while (Xil_In32(FIFO_RDFO)==0){;}
		xil_printf("FIFO_RLR:  0x%08x\n",Xil_In32(FIFO_RLR));
		xil_printf("FIFO_RDR:  0x%08x\n",Xil_In32(FIFO_RDR));
		xil_printf("          FIFO_RDFD:  0x%08x\n",Xil_In32(FIFO_RDFD));
		while ((Xil_In32(FIFO_ISR)&0x04000000)!=0x04000000){;}
		print("write FIFO_ISR\n");
		Xil_Out32(FIFO_ISR, 0x04000000);
    }
    cleanup_platform();
    return 0;
}
