/************************************************************************/
/*																		*/
/*	main.c	--	ZYBO Base System demonstration	 						*/
/*																		*/
/************************************************************************/
/*	Author: Sam Bobrowicz												*/
/*	Copyright 2014, Digilent Inc.										*/
/************************************************************************/
/*  Module Description: 												*/
/*																		*/
/*		This file contains code for running a demonstration of the		*/
/*		Video output and audio capabilities of the ZYBO.				*/
/*																		*/
/************************************************************************/
/*  Revision History:													*/
/* 																		*/
/*		2/25/2014(SamB): Created										*/
/*																		*/
/************************************************************************/

/* ------------------------------------------------------------ */
/*				Include File Definitions						*/
/* ------------------------------------------------------------ */


#include "audio_demo.h"
#include "timer_ps.h"
#include "xparameters.h"
#include "xuartps.h"
#include "xtime_l.h"

/* ------------------------------------------------------------ */
/*				Global Variables								*/
/* ------------------------------------------------------------ */


/*
 * XPAR redefines
 */

#define AUDIO_IIC_ID XPAR_XIICPS_0_DEVICE_ID
#define AUDIO_CTRL_BASEADDR XPAR_AXI_I2S_ADI_1_S_AXI_BASEADDR
#define SCU_TIMER_ID XPAR_SCUTIMER_DEVICE_ID


void MainDemoPrintMenu();
int filter(int* window, int Sample, float* coeff);
void updateWindow(int* window, int Sample);


/* ------------------------------------------------------------ */
/*				Low-Pass and High-Pass FIR filter coefficients									*/
/* ------------------------------------------------------------ */

#define coeffLP -1.692219e-02,5.043750e-02,3.935835e-02,4.341238e-02,5.137933e-02,5.982048e-02,6.748827e-02,7.379049e-02,7.824605e-02,8.052560e-02,8.052560e-02,7.824605e-02,7.379049e-02,6.748827e-02,5.982048e-02,5.137933e-02,4.341238e-02,3.935835e-02,5.043750e-02,-0.0169221860
#define coeffHP -3.942071e-02,-5.929114e-03,1.430997e-02,4.069053e-02,5.762197e-02,4.843584e-02,4.349633e-03,-6.932913e-02,-1.527862e-01,-2.187328e-01,7.562085e-01,-2.187328e-01,-1.527862e-01,-6.932913e-02,4.349633e-03,4.843584e-02,5.762197e-02,4.069053e-02,1.430997e-02,-5.929114e-03,-0.0394207089
#define N 20 // ordine filtro
#define SWITCH_BASE_ADDRESS 0x41220000
#define DDR_BASE_ADDRESS 0x00100000


float LP[]={coeffLP};
float HP[]={coeffHP};




/* ------------------------------------------------------------ */
/*				Procedure Definitions							*/
/* ------------------------------------------------------------ */

void updateWindow(int* window, int Sample){

	int i;
	for(i=0; i<N-1; i++){
		window[N-1-i] = window[N-2-i];
	}
	window[0] = Sample;
}

int filter(int* window, int Sample, float* coeff){

	int i;
	int sum = 0;

	updateWindow(window, Sample);

	for(i=0; i<N; i++){
		sum = sum + window[i]*coeff[i];
	}

	return sum;
}


int main(void)
{


	int i;
	char userInput = 0;
	int SampleL, SampleR, Status;
//	XTime t1 = 0;
//	XTime t2 = 0;
	unsigned int t1 = 0;
	unsigned int t2 = 0;

	Xil_ICacheDisable();
	Xil_L1DCacheDisable();
	Xil_L1ICacheDisable();
	Xil_L2CacheDisable();

	AudioInitialize(SCU_TIMER_ID, AUDIO_IIC_ID, AUDIO_CTRL_BASEADDR);

	Xil_Out32((u32)GLOBAL_TMR_BASEADDR + (u32)GTIMER_CONTROL_OFFSET, (u32)0x1);


	Xil_Out32(AUDIO_CTRL_BASEADDR + I2S_RESET_REG, 0b110); //Reset RX and TX FIFOs
	Xil_Out32(AUDIO_CTRL_BASEADDR + I2S_CTRL_REG, 0b011); //Enable RX Fifo and TX FIFOs, disable mute
	print("ok!\n");

	int windowL[N];
	int windowR[N];
	int windowLBP[N];
	int windowRBP[N];

	for(i=0; i<N; i++){
		windowL[i] = 0;
		windowR[i] = 0;
		windowLBP[i] = 0;
		windowRBP[i] = 0;
	}

	u32 low, low2;
	volatile int *sw = (int *) SWITCH_BASE_ADDRESS;
	volatile int *ddr = (int *) DDR_BASE_ADDRESS;

	xil_printf("Read: %d\n",*ddr);

	while(1)
		{
		SampleL = I2SFifoRead(AUDIO_CTRL_BASEADDR);
		SampleR = I2SFifoRead(AUDIO_CTRL_BASEADDR);

		t2 = t1;
		t1 = Xil_In32(GLOBAL_TMR_BASEADDR + GTIMER_COUNTER_LOWER_OFFSET);

		switch(*sw){
		case 1:	// LP
			XTime_GetTime(&t1);
			//low = Xil_In32(GLOBAL_TMR_BASEADDR + GTIMER_COUNTER_LOWER_OFFSET);
			SampleL = filter(windowL, SampleL, LP);
			//low2 = Xil_In32(GLOBAL_TMR_BASEADDR + GTIMER_COUNTER_LOWER_OFFSET);
			XTime_GetTime(&t1);
			SampleR = filter(windowR, SampleR, LP);
			break;
		case 2: // HP
			SampleL = filter(windowL, SampleL, HP);
			SampleR = filter(windowR, SampleR, HP);
			break;
		case 3: // BP
			SampleL = filter(windowL, SampleL, LP);
			SampleR = filter(windowR, SampleR, LP);
			SampleL = filter(windowLBP, SampleL, HP);
			SampleR = filter(windowRBP, SampleR, HP);
			break;
		default: break;
		}


		I2SFifoWrite(AUDIO_CTRL_BASEADDR, SampleL);
		I2SFifoWrite(AUDIO_CTRL_BASEADDR, SampleR);

//		xil_printf("low1: %d\n",low);
//		xil_printf("low2: %d\n",low2);
//		xil_printf("Durata filter: %d\n",2*(low2-low));
//		xil_printf("T2: %llu\n",t2);
//		xil_printf("T1: %llu\n",t1);
		xil_printf("Durata filter: %llu cicli di clock. \n",2*(t1-t2)); // cicli clock
		}
//	xil_printf("Cicli/sample: %d\n",2*(t1-t2));

	return 0;
}

