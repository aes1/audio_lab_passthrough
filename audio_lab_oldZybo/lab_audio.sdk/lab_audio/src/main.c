/************************************************************************/
/*																		*/
/*	main.c	--	ZYBO Base System demonstration	 						*/
/*																		*/
/************************************************************************/
/*	Author: Sam Bobrowicz												*/
/*	Copyright 2014, Digilent Inc.										*/
/************************************************************************/
/*  Module Description: 												*/
/*																		*/
/*		This file contains code for running a demonstration of the		*/
/*		Video output and audio capabilities of the ZYBO.				*/
/*																		*/
/************************************************************************/
/*  Revision History:													*/
/* 																		*/
/*		2/25/2014(SamB): Created										*/
/*																		*/
/************************************************************************/

/* ------------------------------------------------------------ */
/*				Include File Definitions						*/
/* ------------------------------------------------------------ */


#include "audio_demo.h"
#include "timer_ps.h"
#include "xparameters.h"
#include "xuartps.h"
#include "xtime_l.h"

/* ------------------------------------------------------------ */
/*				Global Variables								*/
/* ------------------------------------------------------------ */


/*
 * XPAR redefines
 */

#define AUDIO_IIC_ID XPAR_XIICPS_0_DEVICE_ID
#define AUDIO_CTRL_BASEADDR XPAR_AXI_I2S_ADI_1_S_AXI_BASEADDR
#define SCU_TIMER_ID XPAR_SCUTIMER_DEVICE_ID

/* ------------------------------------------------------------ */
/*				Low-Pass and High-Pass FIR filter coefficients									*/
/* ------------------------------------------------------------ */

#define coeffLP -1.692219e-02,5.043750e-02,3.935835e-02,4.341238e-02,5.137933e-02,5.982048e-02,6.748827e-02,7.379049e-02,7.824605e-02,8.052560e-02,8.052560e-02,7.824605e-02,7.379049e-02,6.748827e-02,5.982048e-02,5.137933e-02,4.341238e-02,3.935835e-02,5.043750e-02,-0.0169221860
#define coeffHP -3.942071e-02,-5.929114e-03,1.430997e-02,4.069053e-02,5.762197e-02,4.843584e-02,4.349633e-03,-6.932913e-02,-1.527862e-01,-2.187328e-01,7.562085e-01,-2.187328e-01,-1.527862e-01,-6.932913e-02,4.349633e-03,4.843584e-02,5.762197e-02,4.069053e-02,1.430997e-02,-5.929114e-03,-0.0394207089
#define N 20 // ordine filtro
#define SWITCH_BASE_ADDRESS 0x41220000
#define DDR_BASE_ADDRESS 0x00100000


float LP[]={coeffLP};
float HP[]={coeffHP};


int main(void)
{

	int SampleL, SampleR;

	AudioInitialize(SCU_TIMER_ID, AUDIO_IIC_ID, AUDIO_CTRL_BASEADDR);

	Xil_Out32((u32)GLOBAL_TMR_BASEADDR + (u32)GTIMER_CONTROL_OFFSET, (u32)0x1);


	Xil_Out32(AUDIO_CTRL_BASEADDR + I2S_RESET_REG, 0b110); //Reset RX and TX FIFOs
	Xil_Out32(AUDIO_CTRL_BASEADDR + I2S_CTRL_REG, 0b011); //Enable RX Fifo and TX FIFOs, disable mute
	print("Use an AUX-IN jack connector plugged between LINE IN port and a audio source\n");
	//If you have not a AUX-IN cable you can use a microphone (or even headphones, used as rough mic) connected to the LINE IN port (it is important to use the line in port)
	print("Started!\n");

	while(1)
	{

		SampleL = I2SFifoRead(AUDIO_CTRL_BASEADDR);
		SampleR = I2SFifoRead(AUDIO_CTRL_BASEADDR);

		I2SFifoWrite(AUDIO_CTRL_BASEADDR, SampleL);
		I2SFifoWrite(AUDIO_CTRL_BASEADDR, SampleR);

	}

	return 0;
}
