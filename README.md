# Audio LAB: Pass-through

1. On SDK open the workspace for the correct board
2. Connect a Line-in cable from an audio output in the PC or in the smartphone to the LINE IN jack plug in the board
![linein](linein.jpeg)  
3. Connect a pair of headfone to the HPH OUT jack
4. Program the board with the bitstream
5. Run the provvided application. You should hear the audio from the device
